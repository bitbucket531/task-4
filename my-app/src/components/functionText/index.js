import { useState } from "react";
import component from "../../components";
require("../../hoc")

const FunctionText = () => {
  const [txt, setText] = useState("");
  const {Test} = component;
  return (
    <div className="text-wrapper">
      <Test {...{ txt }} />
      <input
        name="name"
        type="text"
        value={txt}
        onChange={(e) => setText(e.target.value)}
      />
    </div>
  );
};

export default FunctionText;
