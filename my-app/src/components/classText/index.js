import React from "react";
import components from "../../components/classIndex";
require("../../hoc/classHoc")
// const Form = () => {
//   return (
//     <form>
//       <label>imie</label>
//       <input name="name" type="text" value={this.state.value} />
//       <input name="send" type="submit" />
//     </form>
//   );
// };

const {Test} = components;
let newDate = new Date();
class ClassText extends React.Component {
    constructor(props){
        super(props);
        this.state={txt:new Date()};
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e){
        this.setState({txt: e.target.value})
        newDate = new Date(e.target.value)
    }

    render() {
    return (
        <div className="text-wrapper">
        <Test {...{newDate}} />
        <input
          name="name"
          type="date"
          value={this.state.txt}
          onChange={this.handleChange}
        />
        </div>
    )
  }
}

export default ClassText;
