import React from "react";

const Test = ({ newDate }) => {
  return (
    <React.Fragment>
      <h1>Wprowadzono date</h1>
      <h1>
        {newDate.getUTCDate() + "/"}
        {newDate.getUTCMonth() + 1 + "/"}
        {newDate.getUTCFullYear()}
      </h1>
    </React.Fragment>
  );
};

export default Test;
