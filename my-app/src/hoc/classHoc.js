import Test from "../components/classTest"
import withCustomText from "./withClassCustom"
import components from "../components/classIndex"

components.Test = withCustomText((props) => <Test {...props} />);
