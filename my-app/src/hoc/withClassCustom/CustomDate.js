import React from "react";

const CustomDate = ({ newDate }) => {
  return (
    <React.Fragment>
      <h1>Wybrałeś kwiecień co wywołało HOC'a</h1>
      <h1>
        {newDate.getUTCDate() + "/"}
        {newDate.getUTCMonth() +1 +"/"}
        {newDate.getUTCFullYear()}
      </h1>
    </React.Fragment>
  );
};

export default CustomDate;
