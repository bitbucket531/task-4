import CustomDate from "./CustomDate";

const withCustomClass = (Component) => (props) => {
  console.log('Custom: '+props.newDate);
  return (
    <div>
      {props.newDate.getUTCMonth() == 3 ? (
        <CustomDate {...props} />
      ) : (
        <Component {...props} />
      )}
    </div>
  );
};

export default withCustomClass;
