import CustomText from "./CustomText";

const withCustomText = (Component) => (props) => {
  return (
    <div>
      {props.txt === "aaa" ? (
        <CustomText {...props} />
      ) : (
        <Component {...props} />
      )}
    </div>
  );
};

export default withCustomText;
