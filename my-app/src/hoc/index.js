import Test from "../components/test"
import withCustomText from "./withCustomText"
import components from "../components"

components.Test = withCustomText((props) => <Test {...props} />);
