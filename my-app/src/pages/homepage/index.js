import React from "react";
import FunctionText from "../../components/functionText";
import ClassText from "../../components/classText"

const Homepage = () => {
  return (
    <div className="main-wrapper">
      <FunctionText />
      <ClassText/>
    </div>
  );
};

export default Homepage;
